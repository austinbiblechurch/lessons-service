const tablesFactory = require('./dynamodb/tables');
const lessonsService = require('./services/lessons');
const graphQLService = require('./services/graphql');

const tables = tablesFactory();
const lessons = lessonsService({ lessonsTable: tables.Lessons });
const graphql = graphQLService({ lessonsService: lessons });

module.exports.handler = (event, context, cb) => {
  console.log('Received event', event);

  const response = {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  };

  if (!event.body) {
    return cb('Query must be provided.');
  }

  const body = JSON.parse(event.body);
  return graphql.runQuery(body)
    .then((result) => {
      response.body = JSON.stringify(result);
      cb(null, response);
    })
    .catch(err => cb(err));
};
