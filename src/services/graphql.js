const stampit = require('stampit');
const graphql = require('graphql');

const schemaFactory = require('../query/schema');

const Logger = require('../logger');

const GraphQLService = stampit()
  .refs({ lessonsService: null })
  .init((opts, data) => {
    if (!opts.lessonsService) throw new Error('lessonsService is required');

    const schema = schemaFactory(opts.lessonsService);
    data.instance.schema = schema; // eslint-disable-line no-param-reassign
  })
  .methods({
    runQuery({ query, variables }) {
      return graphql.graphql(this.schema, query, null, null, variables);
    },
  })
  .compose(Logger);

module.exports = GraphQLService;
