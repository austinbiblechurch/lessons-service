const stampit = require('stampit');

const Logger = require('../logger');

const LessonsService = stampit()
  .refs({ lessonsTable: null })
  .init((opts, data) => {
    if (!opts.lessonsTable) throw new Error('lessonsTable is required');
    data.instance.lessonsTable = opts.lessonsTable; // eslint-disable-line no-param-reassign
  })
  .methods({

    getLessons(args) {
      console.log(`Retrieving records for series: ${args.series}`);
      const cursor = args.cursor ? JSON.parse(args.cursor) : undefined;

      const query = this.lessonsTable.query(args.series)
        .usingIndex('series')
        .where('seriesIndex').gte(0)
        .limit(30);

      if (cursor) {
        query.startKey(cursor);
      }

      return query.execAsync()
        .then(resp => this.logResults(resp))
        .then(resp => this.convertResults(resp))
        .catch((err) => {
          this.log('Error running query', err);
          throw err;
        });
    },

    logResults(resp) {
      this.log('Found', resp.Count, 'items');
      this.log('LastEvaluatedKey', resp.LastEvaluatedKey);
      this.log('Items: ', resp.Items);

      if (resp.ConsumedCapacity) {
        this.log('Query consumed: ', resp.ConsumedCapacity);
      }

      return resp;
    },

    convertResults(resp) {
      const items = resp.Items.map(item => item.attrs);
      const nextCursor = JSON.stringify(resp.LastEvaluatedKey);
      return { items, nextCursor };
    },
  })
  .compose(Logger);

module.exports = LessonsService;
