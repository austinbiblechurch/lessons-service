const dynogels = require('dynogels-promisified');
const Joi = require('joi');

module.exports = () => {
  const lessonsTableName = 'Lessons';

  const Lessons = dynogels.define('Lessons', {
    hashKey: 'location',
    rangeKey: 'range',
    schema: {
      location: Joi.string(),
      range: Joi.string(),
      id: Joi.string(),
      series: Joi.string(),
      title: Joi.string(),
      recordedAt: Joi.date(),
      speaker: Joi.string(),
      seriesIndex: Joi.number(),
      taxonomy: Joi.string(),
      audioFile: Joi.string(),
    },
    tableName: lessonsTableName,
    indexes: [{
      hashKey: 'series', rangeKey: 'seriesIndex', name: 'series', type: 'global',
    }],
  });

  return { Lessons };
};
