const graphql = require('graphql');

const lessonsFactory = require('./lessons');

module.exports = (lessonsService) => {
  const schema = new graphql.GraphQLSchema({
    query: new graphql.GraphQLObjectType({
      name: 'Root',
      description: 'Root of the Schema',
      fields: {
        lessons: lessonsFactory(lessonsService),
      },
    }),
  });

  return schema;
};
