const graphql = require('graphql');

const LessonsType = new graphql.GraphQLObjectType({
  name: 'Lessons',
  fields: {
    location: { type: graphql.GraphQLString },
    recordedAt: { type: graphql.GraphQLString },
    title: { type: graphql.GraphQLString },
    series: { type: graphql.GraphQLString },
    seriesIndex: { type: graphql.GraphQLInt },
    audioFile: { type: graphql.GraphQLString },
  },
});

const LessonsFeed = new graphql.GraphQLObjectType({
  name: 'LessonsFeed',
  fields: {
    nextCursor: { type: graphql.GraphQLString },
    items: {
      type: new graphql.GraphQLList(LessonsType),
    },
  },
});

module.exports = lessonsService => ({
  name: 'LessonsQuery',
  description: 'Retrieve lessons',
  type: LessonsFeed,
  args: {
    series: { type: graphql.GraphQLString },
    cursor: { type: graphql.GraphQLString },
  },
  resolve(parent, args, ast) { // eslint-disable-line no-unused-vars
    console.log('Arguments', args);
    return lessonsService.getLessons(args);
  },
});
